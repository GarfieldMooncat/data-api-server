# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [4 Jan 2023]

### Fixed
- Fix error-handling for requests with no identifer (e.g. `/costumes` endpoint).

## [3 Jan 2023]

### Added
- Request timeout logic, to return a specific response to users if a request is taking too long (likely delays in querying blockchain data)
- Convenience endpoints for requesting different preset MoonCat images without additional query parameters.

### Fixed
- Allow requeust identifiers to end in `.png` or `.json`.

## [14 Dec 2022]

### Changed
- Increased requests-per-minute limit.

## [9 Dec 2022]

### Changed
- Accessories marked as "costumes" updated to include updates from MEAO.

### Fixed
- Default glow for `/image` responses fixed to be the MoonCat's Acclimation status, and sized well by default at small scales.
- Have badly-formatted ID requests return 400 errors rather than 500 errors.

## [2 Dec 2022]
### Added
- Rate-limiting logic, to mitigate the issue of individual users using up large amounts of server resources attempting to iterate through the whole collection rapidly.

### Changed
- Caching infrastructure to handle asynchronous item insertion more robustly, and to fail back to old data if fetching new data fails.

## [22 Oct 2022]

### Fixed
- Add traits for un-rescued ("lunar" and "hero") MoonCats.

## [3 Oct 2022]

### Added
- Ability to visualize just the head/face of a MoonCat.
- Categorization of "costume" accessories and ability to specify rendering them or not.

### Changed
- Back-end infrastructure changed to an ExpressJS engine.