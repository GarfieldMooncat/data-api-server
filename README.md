# MoonCat Data Express

## Swagger Documentation / Spec

You can visit https://editor.swagger.io/ and use the openapi specification from within the `spec.yml` file.

# Development Environment
To run this application server locally:

- Copy the `DOTENV_TEMPLATE.txt` file to `.env`
- Edit `.env` and replace the `API_URL` value with your own URL to an Ethereum node (sign up for a free [Alchemy](https://alchemy.com/?r=6cccf5016cfcb3ed) or [Infura](https://www.infura.io/) account if you don't have one).
- Run `docker-compose build`
- Run `docker-compose up`

This will start a local instance of the Data API server, running on port 25690. You should therefore be able to access it at `http://localhost:25690`.

The server does not live-reload on code changes; if you change any of the source code, you'll need to `CTRL+C` to stop the current instance, re-run `docker-compose build`, and then `docker-compose up` again.

## Deploy to Heroku
The production instance of this application server is hosted on Heroku. Project maintainers can use the Heroku CLI to deploy new versions. To get a working environment for the Heroku CLI scripts within Docker, run:

```
docker pull ubuntu
docker build --no-cache -f Dockerfile.heroku -t heroku-cli .
docker run --rm -it -v ${PWD}:/app -v ${PWD}/.netrc:/root/.netrc -v /var/run/docker.sock:/var/run/docker.sock -w /app heroku-cli
```

Then within that container, to deploy an updated version of the application:

```
$ heroku login -i
$ heroku container:login
$ heroku container:push web --app mooncat-data-api
$ heroku container:release web --app mooncat-data-api
```
