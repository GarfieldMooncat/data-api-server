require('dotenv').config();
const { API_URL, BASE_URL, CONFIG_FILENAME } = process.env;
const LibMoonCat = require('libmooncat');
const Cache = require('./cache');
const Accessory = require('./accessory');
const Lootprint = require('./lootprint');
const DynamicView = require('./dynamic');
const Config = require('../' + CONFIG_FILENAME);

const CACHE_TYPE = 'mooncat';

// For a given scale, what should the glow size be?
// For small scales, these are hard-coded to good-looking values,
// while larger scales are algorithmic
const defaultGlow = {
  1: 3,
  2: 5,
  3: 8,
  4: 11
};

class MoonCat {
  constructor(catId) {
    this.catId = catId;
    this.data = {};
    this.totalAccessories = 0;
    this.accessories = [];
    this.lootprint = null;

    this.rescueOrder = LibMoonCat.getRescueOrder(this.catId);
    this.traits = LibMoonCat.getTraits('erc721', this.catId);
  }

  async init() {
    console.log('Initialising MoonCat', this.catId);
    if (this.rescueOrder == null) {
      // Not a rescued MoonCat; nothing to initialize
      return;
    }

    try {
      this.data = await LibMoonCat.getContractDetails(API_URL, this.catId);

      this.accessories = [];
      this.totalAccessories = await LibMoonCat.getTotalMoonCatAccessories(
        API_URL,
        this.rescueOrder
      );

      for (let i = 0; i < this.totalAccessories; i++) {
        let acc = await LibMoonCat.getMoonCatAccessory(
          API_URL,
          this.rescueOrder,
          i
        );

        Accessory.factory(Number(acc.accessoryId)).catch((err) => {
          // Silently drop these as they will be picked up when actively requested
        }); // Force load Accessory data into cache

        this.accessories.push(acc);
      }

      this.lootprint = await Lootprint.factory(this.rescueOrder);

      if (this.data.catName !== null) {
        // Used extended cache for named mooncats
        Cache.setItemTtl(CACHE_TYPE, this.catId, Config.cache['mooncat-named']);
      }
    } catch (err) {
      console.error('Error in Mooncat.init()', err);
      throw new Error('Error initialising MoonCat!');
    }
  }

  getName() {
    if (this.rescueOrder == null) {
      return `MoonCat ${this.catId}`;
    }
    let name = `MoonCat #${this.rescueOrder}: `;

    switch (this.isNamed()) {
      case 'Yes':
        name += this.data.catName;
        break;
      case 'No':
        name += this.catId;
        break;
      default:
        name += '�';
    }

    if (this.accessories.length > 0) name += ' (accessorized)';

    return name;
  }

  isNamed() {
    if (this.data.catName === null) return 'No';

    if (Config.names.malformed.includes(this.rescueOrder))
      return 'Invalid UTF8';
    if (Config.names.profane.includes(this.rescueOrder)) return 'Redacted';

    return 'Yes';
  }

  getDescription() {
    const link = (this.rescueOrder == null) ? Config.urls.project : `${Config.urls.purrse}/${this.rescueOrder}`;
    return `An Adorable ${this.traits.description.replace(
      'MoonCat',
      `[MoonCat](${link})`
    )}.`;
  }

  getPattern() {
    return (
      this.traits.details.pattern.charAt(0).toUpperCase() +
      this.traits.details.pattern.substring(1)
    );
  }

  getHue() {
    return this.traits.details.hue === 'skyBlue'
      ? 'Sky Blue'
      : this.traits.details.hue.charAt(0).toUpperCase() +
          this.traits.details.hue.substring(1);
  }

  getTotalGlow() {
    return this.traits.details.glow.reduce((sum, g) => sum + g, 0);
  }

  getSaturation() {
    return this.traits.details.isPale ? 'Pale' : 'Normal';
  }

  async getTraits() {
    const identifier = (this.rescueOrder == null) ? this.catId : this.rescueOrder;
    let data = {
      animation_url: `${BASE_URL}/dynamic/${identifier}`,
      description: this.getDescription(),
      license: Config.urls.license,
      name: this.getName(),
      background_color: this.traits.background_color,
      image: `${BASE_URL}/image/${identifier}`,
      details: {
        ...(await this._getDetails()),
        ...this.traits.details,
      },
      attributes: [
        ...this.traits.attributes,
        this._trait('Coat Pattern', this.getPattern()),
        this._trait('Coat Hue', this.getHue()),
        this._trait('Coat Saturation', this.getSaturation()),
        this._trait('isNamed', this.isNamed()),
        ...this._getLootprint(),
        this._trait('Hue', this.traits.details.hueValue, 359),
        this._trait('Total Accessories', this.accessories.length),
        ...(await this._getAccessoryAttributes()),
        ...this._getMoments(),
        ...this._getSpokescat(),
      ],
    };
    if (this.rescueOrder != null) {
      data.external_url = `${Config.urls.purrse}/${this.rescueOrder}`;
    }
    return data;
  }

  async getContractDetails() {
    return {
      rescueIndex: this.rescueOrder,
      catId: this.catId,
      ...(await this._getDetails()),
    };
  }

  async getDynamicHtml() {
    let accessories = [];

    for (let i = 0; i < this.accessories.length; i++) {
      let acc = await Accessory.factory(
        Number(this.accessories[i].accessoryId)
      );

      accessories.push({
        accessoryId: this.accessories[i].accessoryId,
        name: acc.data.name,
        zIndex: this.accessories[i].zIndex,
        verified: acc.metadata.verified,
      });
    }

    accessories.sort((a, b) => a.accessoryId - b.accessoryId);

    return DynamicView(this.catId, this.getTotalGlow(), accessories);
  }

  async _getDetails() {
    return {
      name: this.data.catName,
      isAcclimated: this.data.isAcclimated,
      owner: this.data.owner,
      lootprint:
        this.lootprint !== null && this.lootprint.hasLootprint()
          ? 'Claimed'
          : 'Unclaimed',
      contract: this.data.contract,
      accessories: await this._getAccessories(),
    };
  }

  async _getAccessories() {
    let accessories = [];

    for (let i = 0; i < this.accessories.length; i++) {
      let acc = await Accessory.factory(
        Number(this.accessories[i].accessoryId)
      );
      accessories.push(acc.getSummary(this.accessories[i].zIndex));
    }

    return accessories;
  }

  async _getAccessoryAttributes() {
    let attributes = [];

    for (let i = 0; i < this.accessories.length; i++) {
      let acc = await Accessory.factory(
        Number(this.accessories[i].accessoryId)
      );
      attributes.push(acc.getTrait());
    }

    return attributes;
  }

  _getLootprint() {
    return this.lootprint !== null ? [this.lootprint.getTrait()] : [];
  }

  _getMoments() {
    let moments = [];

    for (let i = 0; i < Config.moments.length; i++) {
      if (Config.moments[i].cats.includes(this.rescueOrder))
        moments.push(
          this._trait('In Moment', `${i}: ${Config.moments[i].name}`)
        );
    }
    return moments;
  }

  _getSpokescat() {
    let spokescat = [];

    if (Object.keys(Config.spokescats).includes(String(this.rescueOrder))) {
      let vm = Config.spokescats[this.rescueOrder];
      spokescat.push(this._trait('MoonCatPop SpokesCat', 'Yes'));
      spokescat.push(this._trait('SpokesCat For', `${vm.id}: ${vm.flavor}`));
    }

    return spokescat;
  }

  _trait(name, value, max) {
    let trait = {
      trait_type: name,
      value,
    };
    if (typeof max !== 'undefined') trait.max_value = max;
    return trait;
  }

  async getImageData(options) {
    try {
      options = options || {};
      let accessories = [];
      let requestedAcc = {};
      let useRequestedAcc = false;

      options.scale = Number(options.scale);
      options.padding = Number(options.padding);

      if (options.scale > 20) options.scale = 20;
      if (options.scale < 1) options.scale = 1;
      if (options.padding > 200) options.padding = 200;
      if (options.padding < 0) options.padding = 0;

      if (typeof options.whitelist == 'undefined') options.whitelist = [];
      if (typeof options.blacklist == 'undefined') options.blacklist = [];

      // How big should the glow be?
      if (typeof options.glowSize !== 'undefined') {
        options.glowSize = Number(options.glowSize); // Allow the request to override with a specific value
      } else {
        if (options.scale < 5) {
          options.glowSize = defaultGlow[options.scale]; // Small scales need precise values to look their best
        } else {
          options.glowSize = 12 + (options.scale - 5);
        }
      }

      // Should the glow be visible at all?
      if (typeof options.glow === 'undefined') {
        options.glow = this.data.isAcclimated; // If unset, set to Acclimation status
      } else {
        options.glow = (options.glow == 'true'); // Force to be boolean
      }

      options.costumes = options.costumes === 'true';

      if (typeof options.acc !== 'undefined') {
        requestedAcc = options.acc
          .split(',')
          .reduce((reqObj, reqAcc, index) => {
            let [accId, accPalette] = reqAcc.split(':');
            reqObj[accId] = {
              paletteIndex:
                typeof accPalette === 'undefined' ? 0 : Number(accPalette),
              zIndex: index + 1,
            };
            return reqObj;
          }, {});
        useRequestedAcc = Object.keys(requestedAcc).length > 0;
      }

      for (let i = 0; i < this.accessories.length; i++) {
        if (
          (options.whitelist.length === 0 ||
            options.whitelist.includes(this.accessories[i].accessoryId)) &&
          !options.blacklist.includes(this.accessories[i].accessoryId) &&
          (options.costumes ||
            !Config.accessories.costumes
              .filter((a) => !options.whitelist.includes(String(a)))
              .includes(Number(this.accessories[i].accessoryId)))
        ) {
          if (
            (!useRequestedAcc && this.accessories[i].zIndex !== 0) ||
            Object.keys(requestedAcc).includes(this.accessories[i].accessoryId)
          ) {
            let acc = await Accessory.factory(
              Number(this.accessories[i].accessoryId)
            );
            let accConfig = useRequestedAcc
              ? requestedAcc[this.accessories[i].accessoryId]
              : this.accessories[i];
            let drawable = acc.getDrawable(
              accConfig.paletteIndex,
              accConfig.zIndex
            );
            if (
              (typeof options.noBackground !== 'undefined' &&
                acc.isBackground()) ||
              (typeof requestedAcc[this.accessories[i].accessoryId] ===
                'undefined' &&
                !acc.metadata.verified)
            )
              drawable = false;
            if (drawable) accessories.push(drawable);
          }
        }
      }

      return LibMoonCat.generateImageWithDimensions(
        this.catId,
        accessories,
        options
      );
    } catch (err) {
      console.log(`Issue generating image for MoonCat ${this.rescueOrder}`);
      console.log(err);
    }
  }

  static factory(catId) {
    if (!Cache.isFresh(CACHE_TYPE, catId) && !Cache.isPending(CACHE_TYPE, catId)) {
      Cache.addItemAsync(CACHE_TYPE, catId, new Promise(async (resolve, reject) => {
        try {
          let mooncat = new MoonCat(catId);
          await mooncat.init();
          resolve(mooncat);
        } catch (err) {
          reject(err);
        }
      }));
    }

    return Cache.getItem(CACHE_TYPE, catId);
  }
}

module.exports = MoonCat;
