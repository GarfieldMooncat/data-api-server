const express = require('express');
const cors = require('cors');
const Response = require('./lib/response');

const app = express();
const port = process.env.PORT || 5000;

app.use(cors({ exposedHeaders: ['X-Image-Width', 'X-Image-Height'] }));
app.use(express.static('public'));
app.use(express.json());

/**
 * Impose a rate-limiting on all requests made to the server.
 * Limit requests to MAX_REQUESTS every TIME_WINDOW milliseconds.
 * If that rate is broken STRIKEOUT times, block the IP for TIMEOUT_LENGTH milliseconds.
 */
const visitTracking = {};
const MAX_REQUESTS = 250;
const TIME_WINDOW = 1000 * 30;
const STRIKEOUT = 400;
const TIMEOUT_LENGTH = 1000 * 60 * 10;
app.use((req, res, next) => {
  const remoteAddressParams = req.socket.remoteAddress.split(':');
  let clientIP = remoteAddressParams[remoteAddressParams.length -1];
  const forwarded = req.get('X-Forwarded-For');
  if (typeof forwarded != 'undefined') {
    // If request was forwarded, assume the last element is the real IP
    let path = forwarded.split(',');
    if (path.length > 2) {
      console.log('Multi-hop request detected', forwarded);
    }
    if (path.length > 1) {
      // Take second-to-last as real IP
      clientIP = path[path.length - 2].trim();
    } else {
      // Take last as real IP
      clientIP = path[path.length - 1].trim();
    }
  }

  const timestamp = Date.now();
  if (typeof visitTracking[clientIP] == 'undefined') {
    // First visit from this IP
    visitTracking[clientIP] = {
      timestamp: timestamp,
      visits: 1,
      strikes: 0,
    };
    return next();
  }

  if (visitTracking[clientIP].strikes >= STRIKEOUT) {
    // That IP is in timeout
    if (timestamp - visitTracking[clientIP].timestamp < TIMEOUT_LENGTH) {
      // Timeout is still active
      visitTracking[clientIP].strikes++;
      res.status(429);
      res.json({ success: 0, message: 'Rate limit reached; penalty enforced', nextAllowed: visitTracking[clientIP].timestamp + TIMEOUT_LENGTH });
      return;
    }
    // Timeout has expired; reset their record
    visitTracking[clientIP] = {
      timestamp: timestamp,
      visits: 1,
      strikes: 0,
    };
    return next();
  }

  // Check for flooding
  if (timestamp - visitTracking[clientIP].timestamp < TIME_WINDOW) {
    // Still within time window of recent request
    visitTracking[clientIP].visits++;
    if (visitTracking[clientIP].visits > MAX_REQUESTS) {
      // Too many requests in this time window
      visitTracking[clientIP].strikes++;
      if (visitTracking[clientIP].strikes == 1) {
        console.log(`Client ${clientIP} started flooding; first rate-limit warning given`);
      } else if (visitTracking[clientIP].strikes >= STRIKEOUT) {
        console.log(`Client ${clientIP} has exceeded the flood limit; timing them out`);
      }
      res.status(429);
      res.json({ success: 0, message: 'Rate limit reached', nextAllowed: visitTracking[clientIP].timestamp + TIME_WINDOW });
      return;
    }
    return next();
  }

  // Outside the time window of recent requests; reset their tracking
  visitTracking[clientIP] = {
    timestamp: timestamp,
    visits: 1,
    strikes: 0,
  };
  return next();
});

/**
 * Impose a processing limit on the application processing.
 * If processing on the server takes more than 20 seconds, respond to the client that processing is taking too long.
 * Note, this does not stop the processing, so likely the result will still be done/cached at some future point.
 * This has to be under 30 seconds as Heroku imposes a hard cap on response time length.
 */
const RESPONSE_TIMEOUT_LIMIT = 20 * 1000;
app.use((req, res, next) => {
  res.setTimeout(RESPONSE_TIMEOUT_LIMIT, () => {
    if (res.headersSent) return;
    console.log('Response timeout hit!');
    res.status(424);
    res.json({ success: 0, message: 'Timed out' });
  });
  return next();
});

/**
 * Validate MoonCat IDs passed as query parameters.
 * API endpoints allow specifying a MoonCat by rescue order (a decimal number between 0 and 25439),
 * or by hex identifier (a five-byte (ten-character) hexadecimal value). The hex identifier can be
 * prefixed with "0x" or not.
 */
app.param('id_or_idx', function (req, res, next, id) {
  if (
    id.match(
      /^(?:[0-9]{0,5}|0x00[0-9a-f]{8}|0xff[0-9a-f][0-9a-f]000ca7|00[0-9a-f]{8}$|^ff[0-9a-f][0-9a-f]000ca7)(?:\.(?:png|json))?$/
    ) === null
  ) {
    res.status(400).send('Invalid MoonCat Id or RescueIndex');
    return;
  } else {
    next();
  }
});

app.get('/traits/:id_or_idx', async (req, res, next) => {
  let response = new Response('mooncat', req.params.id_or_idx);
  let traits = await response.getTraits();

  if (res.headersSent) return; // Check if timed-out
  if (traits instanceof Error) next(traits);
  else res.json(traits);
});

app.get('/contract-details/:id_or_idx', async (req, res, next) => {
  let response = new Response('mooncat', req.params.id_or_idx);
  let details = await response.getContractDetails();

  if (res.headersSent) return; // Check if timed-out
  if (details instanceof Error) next(details);
  else res.json(details);
});

app.options('/contract-details', cors());

app.post('/contract-details', cors(), async (req, res, next) => {
  let response = new Response('mooncat');
  let details = await response.getContractDetailsSet(req.body);

  if (res.headersSent) return; // Check if timed-out
  if (details instanceof Error) next(details);
  else res.json(details);
});

app.get('/dynamic/:id_or_idx', async (req, res, next) => {
  let response = new Response('mooncat', req.params.id_or_idx);
  let html = await response.getDynamicHtml();

  if (res.headersSent) return; // Check if timed-out
  if (html instanceof Error) next(html);
  else {
    res.writeHead(200, {
      'Content-Type': 'text/html',
    });
    res.end(html);
  }
});

app.get('/image/:id_or_idx', async (req, res, next) => {
  let response = new Response('mooncat', req.params.id_or_idx);

  let whitelist =
    typeof req.query['acc-inc'] === 'string'
      ? req.query['acc-inc'].split(',')
      : [];
  let blacklist =
    typeof req.query['acc-exc'] === 'string'
      ? req.query['acc-exc'].split(',')
      : [];

  let imgData = await response.getImageData({
    ...Object.fromEntries(
      Object.entries(req.query).filter(([key]) =>
        [
          'scale',
          'padding',
          'backgroundColor',
          'fullSize',
          'noCat',
          'headOnly',
          'noBackground',
          'glow',
          'glowSize',
          'glowOpacity',
          'acc',
          'costumes',
        ].includes(key)
      )
    ),
    whitelist,
    blacklist,
  });

  if (res.headersSent) return; // Check if timed-out
  if (imgData instanceof Error) next(imgData);
  else makeImageResponse(response, imgData, res);
});

app.get('/cat-image/:id_or_idx', async (req, res, next) => {
  let response = new Response('mooncat', req.params.id_or_idx);
  let imgData = await response.getImageData({ acc: '' });

  if (imgData instanceof Error) next(imgData);
  else {
    makeImageResponse(response, imgData, res);
  }
});

app.get('/regular-image/:id_or_idx', async (req, res, next) => {
  let response = new Response('mooncat', req.params.id_or_idx);
  let imgData = await response.getImageData({ acc: '', glow: 'false' });

  if (res.headersSent) return; // Check if timed-out
  if (imgData instanceof Error) next(imgData);
  else makeImageResponse(response, imgData, res);
});

app.get('/glow-image/:id_or_idx', async (req, res, next) => {
  let response = new Response('mooncat', req.params.id_or_idx);
  let imgData = await response.getImageData({ acc: '', glow: 'true' });

  if (imgData instanceof Error) next(imgData);
  else makeImageResponse(response, imgData, res);
});

app.get('/face-image/:id_or_idx', async (req, res, next) => {
  let response = new Response('mooncat', req.params.id_or_idx);
  let imgData = await response.getImageData({ acc: '', headOnly: 'true' });

  if (res.headersSent) return; // Check if timed-out
  if (imgData instanceof Error) next(imgData);
  else makeImageResponse(response, imgData, res);
});

app.get('/accessorized-image/:id_or_idx', async (req, res, next) => {
  let response = new Response('mooncat', req.params.id_or_idx);

  let whitelist =
    typeof req.query['acc-inc'] === 'string'
      ? req.query['acc-inc'].split(',')
      : [];
  let blacklist =
    typeof req.query['acc-exc'] === 'string'
      ? req.query['acc-exc'].split(',')
      : [];

  let imgData = await response.getImageData({
    acc: req.query['acc'],
    costumes: true,
    whitelist,
    blacklist
  });

  if (imgData instanceof Error) next(imgData);
  else makeImageResponse(response, imgData, res);
});

app.get('/cat-walk/:id_or_idx', (req, res, next) => {
  let response = new Response('mooncat', req.params.id_or_idx);
  let rescueOrder = response.getRescueOrder();
  if (rescueOrder == null) {
    res.status(400).send('Not a Rescued MoonCat');
    return;
  }

  res.set('location', `https://ipfs.io/ipfs/bafybeib5iedrzr7unbp4zq6rkrab3caik7nw7rfzlcfvu4xqs6bfk7dgje/${rescueOrder}.png`);
  res.status(301).send();
});

app.get('/accessory/:id', async (req, res, next) => {
  let response = new Response('accessory', req.params.id);
  let accessory = await response.getAccessory();

  if (res.headersSent) return; // Check if timed-out
  if (accessory instanceof Error) next(accessory);
  else res.json(accessory);
});

app.get('/accessory-image/:id', async (req, res, next) => {
  let response = new Response('accessory', req.params.id);
  let base64Data = await response.getAccessoryImageData();

  if (res.headersSent) return; // Check if timed-out
  if (base64Data instanceof Error) next(base64Data);
  else {
    let img = response.getImgFromData(base64Data);

    res.writeHead(200, {
      'Content-Type': 'image/png',
      'Content-Length': img.length,
    });
    res.end(img);
  }
});

app.get('/costumes', (req, res, next) => {
  let response = new Response('static');
  let costumes = response.getCostumes();

  if (res.headersSent) return; // Check if timed-out
  if (costumes instanceof Error) next(costumes);
  else res.json(costumes);
});

app.options('/invalidate', cors());

app.post('/invalidate', cors(), (req, res) => {
  let response = new Response('static');

  response.invalidateCacheItem(req.query.type, req.query.key);
  if (res.headersSent) return; // Check if timed-out
  res.send('OK');
});

app.use((err, req, res, next) => {
  if (req.xhr) {
    res.status(500).send({ error: 'There was an error.' });
  } else {
    next(err);
  }
});

app.use((err, req, res, next) => {
  console.error('Internal error hit', err);
    res.status(500).send('ERROR');
});

app.listen(port, () => {
  console.log(`MoonCat Data Express app listening on port ${port}`);
});

function makeImageResponse(response, imgData, res) {
  let img = response.getImgFromData(imgData.img);

  res.writeHead(200, {
    'Content-Type': 'image/png',
    'Content-Length': img.length,
    'X-Image-Width': imgData.width,
    'X-Image-Height': imgData.height,
  });
  res.end(img);
}
